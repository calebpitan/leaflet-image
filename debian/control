Source: leaflet-image
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Build-Depends:
 brotli,
 debhelper-compat (= 12),
 node-d3-queue,
 pandoc <!nodoc>,
 pigz,
 rename,
 webpack,
Standards-Version: 4.5.1
Homepage: https://github.com/mapbox/leaflet-image
Vcs-Browser: https://salsa.debian.org/js-team/leaflet-image
Vcs-Git: https://salsa.debian.org/js-team/leaflet-image.git
Rules-Requires-Root: no

Package: node-leaflet-image
Architecture: all
Depends:
 node-d3-queue,
 nodejs,
 ${misc:Depends},
Enhances:
 node-leaflet,
Description: image export for Leaflet - Node.js library
 Leaflet-image is a plugin for the Leaflet JavaScript library,
 for exporting images out of Leaflet maps without a server component
 by using Canvas and CORS.
 .
 Leaflet is a JavaScript library for mobile-friendly interactive maps.
 .
 This package provides Leaflet-image library
 usable with Node.js - an event-based server-side JavaScript engine.

Package: libjs-leaflet-image
Architecture: all
Depends:
 ${misc:Depends},
Recommends:
 javascript-common,
 libjs-leaflet,
Enhances:
 libjs-leaflet (>= 1),
Multi-Arch: foreign
Description: image export for Leaflet - browser library
 Leaflet-image is a plugin for the Leaflet JavaScript library,
 for exporting images out of Leaflet maps without a server component
 by using Canvas and CORS.
 .
 Leaflet is a JavaScript library for mobile-friendly interactive maps.
 .
 This package provides Leaflet-image library
 directly usable in web browsers.
