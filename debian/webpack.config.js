'use strict';

var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {

	target: 'web',
	devtool: 'source-map',
	resolve: {
		modules: ['/usr/lib/nodejs', '/usr/share/nodejs'],
	},
	resolveLoader: {
		modules: ['/usr/lib/nodejs', '/usr/share/nodejs'],
	},
	node: false,
	output: {
		libraryTarget: 'umd'
	},
}

module.exports = config;
